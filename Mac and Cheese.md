 * 1 personal bowl filled 3/4 of the way with Macaroni
 * 50 ml Milk
 * 1 tsp Sodium Citrate **OR** 2 American Cheese Slices
 * 90 g Cheddar or Gouda
 * 10 g Parmesan
 * 1 tsp Salt
 * 1 tsp Paprika (optional)
 * 1/2 tsp Mustard Powder (optional)
 * 1/2 tsp Garlic Powder (optional)
 * Boil noodles
 * While noodles are boiling, add 50 ml milk and 1 tsp sodium citrate to a pan and heat slowly. Add cheese and other spices and stir constantly
 * Add noodles to sauce once cheeses are incorporated
