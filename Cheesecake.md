Almond Flour Cheesecake Crust

 * 250 g Wholesome Yum Blanched Almond Flour
 * 75 ml Butter, metled
 * 3 tbsp Erythritol (granular or powdered works fine)
 * 1 tsp Vanilla extract

Keto Cheesecake Filling

 * 900 g Cream cheese (softened)
 * 300 ml Besti Powdered Erythritol (erythritol must be powdered; can also use powdered monk fruit sweetener)
 * 3 Eggs
 * 1 tbsp Lemon juice
 * 1 tsp Vanilla extract

 * Stir the almond flour, melted butter, erythritol, and vanilla extract in a medium bowl, until well combined
 * Press into bottom of springform pan
 * Beat cream cheese and powdered sweetener together until fluffy, add eggs one at a time. Add lemon juice and vanilla extract
 * Pour filling onto crust, smooth the top
 * Add water to instant pot, set on a trivet and pressure cook for 25 minutes, let naturally release
 * Cool for 15 minutes on a wire rack, then refrigerate for one hour

See https://www.wholesomeyum.com/recipes/low-carb-cheesecake-keto-gluten-free-sugar-free/
