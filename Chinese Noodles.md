 * 100g Flour
 * 1 Egg
 * 1 tbsp Oyster Sauce
 * 1 tbsp Soy Sauce
 * 1 tbsp Dark Soy Sauce
 * 1 tbsp Rice Vinegar
 * Bell Peppers, Onions, Carrots
 * 1 Green Onion
 * 1 clove Garlic
 * Make noodles from flour and egg, boil briefly and rinse with cold water
 * Cook bell peppers, onions, carrots in smoking hot sesame oil, set aside
 * Cook noodles in sesame oil, add mix of sauces and vinegar
 * Re-add cooked vegetables
 * Add green onion and garlic and cook briefly
