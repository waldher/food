 * 300 ml Milk
 * Heat until 30 degrees
 * 1 packet Yeast
 * Combine and let sit for 5 mins
 * 2 Eggs
 * 110 g Butter
 * 50 g Sugar
 * 1 tsp Salt
 * Combine with mixer
 * 600 g Flour
 * Add half of flour to mixer
 * Add other half of flour to mixer
 * Let rise in a bowl for 1 hour
 * Roll to 0.5 inches thick
 * Deep Fry
