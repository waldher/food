 * 200 ml Arborio Rice
 * 200 g can Diced Tomatoes
 * 400 ml Vegetable Broth
 * 4 Tbsp Grated Parmesan
 * 1 small Onion, Diced
 * 1 clove Garlic, Diced
 * 1 Tbsp Butter
 * Boil broth, let simmer
 * Lightly fry onions in butter, add rice, fry until just shimmering, add garlic, salt, pepper, spices to taste, add can of tomatoes
 * Wait until tomato juice absorbed, add broth one ladle at a time as it becomes absorbed over the course of 15 minutes. Stir frequently
 * Add grated parmesan
