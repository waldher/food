 * 1.5 tsp Yeast
 * 60 ml Warm Water
 * 1.5 tsp Sugar
 * Mix and let activate for 5 mins
 * 200g Flour
 * 1.5 tsp Salt
 * 250 ml Warm Water
 * Mix the previous three in mixer (dough hook)
 * Add yeast mixture to flour mixture
 * 220g Flour, add to mixer
 * 2 tbsp Butter, add 1 tbsp at a time to mixer
 * Mix until dough is consistent
 * Knead by hand
 * Let rise in a buttered bowl for 45 minutes, with oiled plastic wrap covering in a warm place
 * Punch down, form a square, and place into a bread pan, let rise for 45 minutes
 * Bake 220 C in oven for 15 minutes
 * Turn oven down to 190 C for 45 minutes
 * Remove from bread pan immediately
