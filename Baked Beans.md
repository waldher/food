 * 50 ml water
 * 1 medium onion, diced
 * 2 teaspoons smoked paprika
 * 1 heaping teaspoon garlic powder
 * 400g dried small white beans (navy or great northern) (2 cups), rinsed and odd beans removed (not soaked)
 * 1 liter low-sodium vegetable broth (or half water + broth)
 * 75 ml pure maple syrup
 * 50 ml tomato paste (use a 6oz. can for extra tomato flavor)
 * 50 ml apple cider vinegar
 * 2 tablespoons mustard (regular, dijon or whole grain)
 * ½ teaspoon fresh ground pepper
 * 2 bay leaves
 * good pinch of mineral salt, or to taste
 * Saute water and onion
 * Add paprika and garlic powder
 * Add all other ingredients, stir
 * Pressure cook on high for 75 minutes, then let naturally release
 * Remove bay leaves

Source: https://simple-veganista.com/healthy-baked-beans-instant-pot/
