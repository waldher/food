 * 200 g ground Beef and/or Pork
 * Vegetables of choice (carrots, celery, onions)
 * 200 ml Water
 * 1 tsp Vegetable Bouillon
 * 10 ml Balsamic Vinegar
 * Tomato Paste
 * Basil
 * Rosemary
 * Simmer for 45 minutes
