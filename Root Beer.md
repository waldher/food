# Root Beer

* 500 ml Cold, Carbonated Water (carbonate it as much as you can)
* Sweetener (see below)
* Syrup (see below)

## Sweetener

Somewhere between the equivalent of 40g and 50g of sugar is needed.

**Liquid Cyclamate with Saccharin** has given me proven results. Approximately 4.0g was about right.

**Powdered Sucralose** tried with 5 heaping teaspoons (approximately 5g) was a bit too sweet, try less.

## Syrup

### Jing Soda Root Beer Cream

https://www.amazon.com/dp/B094CGCBL2

Three full droppers worth was a bit flavorless, but there's potential here. Tried 4.0g.

### McCormick Root Beer Concentrate

This lead to perfect results, but it's hard to find.
