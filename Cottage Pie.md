 * 1 small onion, diced
 * 1 large carrot, diced
 * 1 stalk celery, diced
 * Fry the above in oil
 * 500g ground beef
 * 1 tbsp Tomato paste
 * 2 tbsp Worchestershire Sauce
 * 2 tbsp Rosemary or Thyme
 * Add the above and continue frying
 * 200 ml Stock
 * Add to the pot
 * 600g peeled potatoes in large pieces
 * Add potatoes in a basket to the pot
 * Pressure cook for 12 minutes
 * Remove potatoes, mash with 1 cup milk and 1 tbsp worchestershire sauce
 * 100g peas
 * Add peas to beef and transfer to baking dish
 * Add mashed potatoes on top
 * Broil 15 minutes
