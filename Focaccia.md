 * 300ml warm water
 * 1.5 tsp dry yeast
 * 1 tsp honey
 * Stir the above together and set aside
 * 500g Flour Type 00
 * 1.5 tsp salt
 * Combine salt and flour in a food processor
 * 3 tbsp Olive Oil
 * Add water/yeast and olive oil to food processor and process until dough is consistent and not sticky
 * Oil a large bowl and let the dough ball rise for 1 hour, lightly covered
 * Oil and salt a large baking sheet, then punch down the dough and spread it evenly across the sheet. Cover and let rise for 30 minutes
 * Make dimples in the dough with a finger and pour salt water into the dimples. Drench the dough with oil
 * Bake at 200 C for 30 minutes, or until moderately brown
