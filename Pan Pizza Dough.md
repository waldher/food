 * 400 g Flour
 * 4 g Yeast (1 packet)
 * 275 g Water
 * 10 g Salt
 * 8 g Olive Oil
 * Loosely combine and let rise in an oiled, covered bowl for 8 to 24 hours
 * Turn into a ball, put into oiled cast iron skillet and let rise in skillet for 2 hours
 * Cover in sauce and toppings and bake at max temp in oven until cheese is the right color
