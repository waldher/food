 * https://www.budgetbytes.com/taco-seasoning/
 * 1 Tbsp chili powder ($0.30)
 * 1 tsp smoked paprika ($0.10)
 * 1 tsp cumin ($0.10)
 * 1/4 tsp cayenne pepper ($0.02)
 * 1/2 tsp oregano ($0.05)
 * 1/2 tsp salt ($0.05)
 * 1/2 tsp 15 cranks fresh black pepper ($0.05)
 * 1 lb beef
 * Optional add-ins: 1/4 tsp garlic powder, 1/4 tsp onion powder (either or both, depending on if you are using fresh garlic or onion); 1/2 tsp cornstarch (add 1/3 cup water to your meat or beans to create a saucy consistency).
